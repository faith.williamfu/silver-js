export default class ReceiptPrinter {
  constructor (products) {
    this.products = [...products];
  }

  print (barcodes) {
    // TODO: Please implement the method
    // <-start-
    const barcodesOrdered = barcodes.sort();
    const barcodesWithoutDuplication = [];
    const productBarcode = this.products.map(name => name.barcode);
    const productName = this.products.map(name => name.name);
    const productPrice = this.products.map(name => name.price);
    const productUnit = this.products.map(name => name.unit);
    for (let i = 0; i < barcodesOrdered.length; i++) {
      if (barcodesWithoutDuplication.indexOf(barcodesOrdered[i]) === -1) {
        barcodesWithoutDuplication.push(barcodesOrdered[i]);
      }
    }
    const barcodesCount = new Array(barcodesWithoutDuplication.length);
    for (let l = 0; l < barcodesCount.length; l++) {
      barcodesCount[l] = 0;
    }
    for (let j = 0; j < barcodesWithoutDuplication.length; j++) {
      for (let k = 0; k < barcodesOrdered.length; k++) {
        if (barcodesWithoutDuplication[j] === barcodesOrdered[k]) {
          barcodesCount[j]++;
        }
      }
    }
    let printContent = '';
    let printPrice = 0;
    if (barcodesOrdered.length === 0) {
      printContent = '\n';
    } else {
      for (let i = 0; i < barcodesWithoutDuplication.length; i++) {
        if (!productBarcode.includes(barcodesWithoutDuplication[i])) {
          throw new Error('Unknown barcode.');
        }
      }
      for (let i = 0; i < barcodesWithoutDuplication.length; i++) {
        printContent += productName[productBarcode.indexOf(barcodesWithoutDuplication[i])];
        for (let j = String(productName[productBarcode.indexOf(barcodesWithoutDuplication[i])]).length; j < 30; j++) {
          printContent += ' ';
        }
        printContent += 'x';
        printContent += barcodesCount[i];
        printContent += '        ';
        printContent += productUnit[productBarcode.indexOf(barcodesWithoutDuplication[i])];
        printContent += '\n';
        printPrice += barcodesCount[i] * productPrice[productBarcode.indexOf(barcodesWithoutDuplication[i])];
      }
    }
    const receipt =
    '==================== Receipt ====================\n' +
    printContent +
    '\n' +
    '===================== Total =====================\n' +
    printPrice.toFixed(2);
    return receipt;
    // --end->
  }
}
